/**
 * Contact 
 * @param {number} index - Unique id
 * @param {string} firstName - Contact first name
 * @param {string}  lastName - Contact last name
 * @param {Object}  params - Optional fields for contact
 * @returns {Contact}
 */
const Contact = function(index, firstName, lastName, params){
    this.id = index;
    this.firstname = firstName;
    this.lastname = lastName;
    if (params !== undefined) {
        if (Object.prototype.hasOwnProperty.call(params, "phone")) this.phone = params.phone;
        if (Object.prototype.hasOwnProperty.call(params, "address")) this.address = params.address;
        if (Object.prototype.hasOwnProperty.call(params, "email")) this.email = params.email;
        if (Object.prototype.hasOwnProperty.call(params, "notes")) this.notes = params.notes;
    }
};
export { Contact };