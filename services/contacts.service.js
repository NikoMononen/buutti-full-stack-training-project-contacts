"use strict";
import { Contact } from "../models/contact.model.js";
import { Group } from "../models/group.model.js";

/** 
 *  Class representing a contacts book
 */
class ContactsService {
    /** @private */
    #databaseHandler;

    /** @private */
    #contactsObject;

    /** @private */
    #groupsObject;

    /** 
     * Constructs ContactsService
     * @param {DatabaseHandler} databaseHandler - instance of DatabaseHandler class that handles database calls 
     */
    constructor(databaseHandler){
        this.#databaseHandler = databaseHandler;
        this.#contactsObject = this.#databaseHandler.getContacts();
        this.#groupsObject = this.#databaseHandler.getGroups();
    }

    /**
     * Checks if string is defined and at least one character long
     * @private
     * @param {string} string
     * @returns {boolean}
     */
    #checkString = function(string){
        if (string === undefined) return false;
        return string.length > 1;
    };

    /**
     * Add contact
     * @param {string} firstName
     * @param {string} lastName
     * @param {Object} params - Additional fields for contact
     * @returns {Contact} Added contact
     */
    addContact(firstName, lastName, params){
        if (!this.#checkString(firstName) || !this.#checkString(lastName)) return null;
        const contactsObjectKeys = Object.keys(this.#contactsObject);
        const index = contactsObjectKeys.length > 0 ? Math.max(...contactsObjectKeys) + 1 : 1;
        const contact = new Contact(index, firstName, lastName, params);
        this.#contactsObject[contact.id] = contact;
        return contact;
    }
    
    /**
     * Add group
     * @param {string} group
     * @returns {Group} Added contact
     */
    addGroup(name){
        if (!this.#checkString(name)) return null;
        const groupsObjectKeys = Object.keys(this.#groupsObject);
        const index = groupsObjectKeys.length > 0 ? Math.max(...groupsObjectKeys) + 1 : 1;
        const group = new Group(index, name);
        this.#groupsObject[group.id] = group;
        return group;
    }
    /**
     * Get Contact 
     * @param {number} id - UID of the contact
     * @returns {Contact}
     */
    getContact(id){
        const intId = parseInt(id);
        if (isNaN(intId) || !(Object.prototype.hasOwnProperty.call(this.#contactsObject, intId.toString()))) return null;
        const contact = this.#contactsObject[intId];
        return contact;
    }
    /**
     * Get group 
     * @param {number} id - UID of the group
     * @returns {Group}
     */
    getGroup(id){
        const intId = parseInt(id);
        if (isNaN(intId) || !(Object.prototype.hasOwnProperty.call(this.#groupsObject, intId.toString()))) return null;
        const group = this.#groupsObject[intId];
        return group;
    }

    /**
     * Returns all groups
     * @returns Array.<{Group}>
     */
    getAllGroups() {
        return Object.values(this.#groupsObject);
    }

    // Returns all contacts. (#contactsObject)
    getAllContacts() {
        return Object.values(this.#contactsObject);
    }

    /**
     * Remove group
     * @param {number} id
     * @returns {boolean} Whether the operation succeeded or not
     */
    removeGroup(id){
        const intId = parseInt(id);
        if (isNaN(intId) || !(Object.prototype.hasOwnProperty.call(this.#groupsObject, intId.toString()))) return false;
        delete this.#groupsObject[intId.toString()];
        return true;
    }

    /**
     * Add contact to group
     * @param {number} contact - UID of the contact to add
     * @param {number} groupId - UID of the group
     * @returns {boolean} Whether the operation succeeded or not
     */
    addContactToGroup(contactId, groupId){
        if (!(Object.prototype.hasOwnProperty.call(this.#groupsObject, groupId)) || !(Object.prototype.hasOwnProperty.call(this.#contactsObject, contactId))) return false;
        const group = this.#groupsObject[groupId];
        if (group.contacts.includes(contactId)) return false;
        group.contacts.push(contactId);
        return true;
    }

    /**
     * Remove contact from group
     * @param {number} contact - UID of the contact to add
     * @param {number} groupId - UID of the group
     * @returns {boolean} Whether the operation succeeded or not
     */
    removeContactFromGroup(contactId, groupId){
        if ( !(Object.prototype.hasOwnProperty.call(this.#groupsObject, groupId)) || !(Object.prototype.hasOwnProperty.call(this.#contactsObject, contactId))) return false;
        const group = this.#groupsObject[groupId];
        const indexOfContact = group.contacts.indexOf(contactId);
        if (indexOfContact === -1) return false;
        group.contacts.splice(indexOfContact,1);
        return true;
    }

    /**
     * Saves contacts and groups
     */
    save = function(){
        this.#databaseHandler.saveContacts(this.#contactsObject);
        this.#databaseHandler.saveGroups(this.#groupsObject);
    };

    searchContact(params) {
        if (params === null || params === undefined) return null;
        const arr = Object.entries(params);
        const contacts = Object.values(this.#contactsObject);
        const results = [];
        contacts.forEach(contact => {
            for(let i = 0; i < arr.length; i++) {
                const [key, value] = arr[i]; 
                if (Object.prototype.hasOwnProperty.call(contact, key)) { 
                    if (contact[key].toLowerCase().includes(value.toLowerCase())) {
                        results.push(contact);
                        break;
                    }
                }
            }
        });
        return results;
    }

    /**
     * Get all group ids where contact belongs to
     * @param {number} contactId
     * @returns Array.<{number}>
     */
    getContactGroups(contactId){
        const contactGroups = Object.values(this.#groupsObject).reduce((groups,group) =>{
            if (group.contacts.includes(contactId)) groups.push(group.id);
            return groups;
        },[]);
        return contactGroups;
    }

    /**
     * Removes given contact. Returns whether the operation was successfully or not
     * @date 2022-02-24
     * @param {number} id - UID of the contact
     * @returns {boolean}
     */
    removeContact(id) {
        const intId = parseInt(id);
        if(isNaN(intId)) return false;
        if(Object.prototype.hasOwnProperty.call(this.#contactsObject, id)) {
            // remove contact from all groups where the contact belongs
            const contactGroups = this.getContactGroups(intId);
            for (let i = 0; i < contactGroups.length; i++) {
                const group = contactGroups[i];
                this.removeContactFromGroup(intId, group.id);
            }
            // Delete contact
            delete this.#contactsObject[id];
            return true;
        } 
        else return false;
    }
    updateContact(id, params) {
        const intId = parseInt(id);
        if(isNaN(intId) || !(Object.prototype.hasOwnProperty.call(this.#contactsObject, id))) return null;
        const contact = this.#contactsObject[id];
        const array = Object.entries(params); // [[id,1],[firstname, "teppo"]]
        array.forEach(([key, value]) => {
            switch(key) {
                case "firstname":
                    contact.firstname = value;
                    break;
                case "lastname":
                    contact.lastname = value;
                    break;
                case "phone":
                    contact.phone = value;
                    break;
                case "address":
                    contact.address = value;
                    break;
                case "email":
                    contact.email = value;
                    break;
                case "notes":
                    contact.notes = value;
                    break;
            }
        });
        return contact;
    }
    updateBatch(id, params){
        const intId = parseInt(id);
        if (isNaN(intId) || !(Object.prototype.hasOwnProperty.call(this.#groupsObject, intId.toString()))) return false; 
        const group = this.#groupsObject[intId];
        group.contacts.forEach(contactId => {
            if(this.updateContact(contactId, params) === null) {
                return false;
            }
        });
        return true;
    }
    // Sorts given contactsArray (array of contactObjects), by sortField and sortOrder
    // Returns sorted array of contactObjects.
    sortContacts(contactsArray, sortField, sortOrder) {
        // contactArray has to be an array, with something in it
        if (!Array.isArray(contactsArray) || contactsArray.length < 1) { 
            console.log("Nothing to sort.");
            return null;
        }

        // sortField has to exist in at least one of the contacts in contactArray
        const sortFieldExists = contactsArray.some(obj => Object.hasOwn(obj, sortField));
        if (!sortFieldExists) {
            console.log("Missing or invalid sorting key.");
            return null;
        }
        
        // sorting order has to be "ASC" or "DESC"
        // sortingOrder is used in actual compare function
        let sortingOrder = 1;
        if (sortOrder === "ASC") { 
            sortingOrder = 1;
        } else if (sortOrder === "DESC") {
            sortingOrder = -1;
        } else {
            console.log("Invalid sort order");
            return null;
        }

        function compare (sortField) { 
            // Actual sort function. Returns sort result modified by sortingOrder
            return function (a,b) {
                // Contacts without given sortField (=== undefined) will be sorted to last
                if (b[sortField] !== undefined) {
                    if (a[sortField] < b[sortField]) {
                        return -1 * sortingOrder;
                    } else if (a[sortField] > b[sortField]) {
                        return 1 * sortingOrder;
                    } else {
                        return 0;
                    }
                } else {
                    return -1;
                }
            };
        }

        const sortedContacts = contactsArray.sort(compare(sortField, sortOrder));
        return sortedContacts;
    }       
}

export { ContactsService };